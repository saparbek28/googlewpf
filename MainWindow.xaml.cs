﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace GoogleWPF
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private string url = "https://www.google.com"; //"http://www.youtube.com/index?hl=ru";
        public MainWindow()
        {
            InitializeComponent();
            (tabs.Items[0] as TabItem).Content = new System.Windows.Controls.WebBrowser
            {
                Source = new Uri(url),
            };
        }

        private void CloseTab(object sender, RoutedEventArgs e)
        {
            for (int i = 0; i < tabs.Items.Count; i++)           
                if (((tabs.Items[i] as TabItem).Header as StackPanel) != null && ((((tabs.Items[i] as TabItem).Header as StackPanel).Children[1] as Button) == (sender as Button)))
                    tabs.Items.Remove(tabs.Items[i]);     
        }

        private void AddTab(object sender, RoutedEventArgs e)
        {
            StackPanel stackPanel = new StackPanel {
                Orientation = Orientation.Horizontal
            };
            stackPanel.Children.Add(new Label {
                Content = "Google"
            });

            TabItem tabitem = new TabItem()
            {
                Header = stackPanel
            };
            tabitem.Content = new System.Windows.Controls.WebBrowser {
                Source = new Uri(url)
            };
            tabs.Items.Insert(tabs.Items.Count - 1, tabitem);
            tabs.SelectedItem = tabs.Items[tabs.Items.Count - 2];

            Button button = new Button();
            button.Content = "x";
            button.Click += CloseTab;
            stackPanel.Children.Add(button);

        }
    }
}
